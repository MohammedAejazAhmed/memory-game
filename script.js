const gameContainer = document.getElementById("game");
let prevColor = 'none';
let prevId = 'none';
let lock = 0;
let start = 0;
let restart = 0;
let successfulClick = 0;
let score = 0;
let bestScore = 0;
let count = 0;
const COLORS = [
  "red",
  "blue",
  "green",
  "orange",
  "purple",
  "red",
  "blue",
  "green",
  "orange",
  "purple"
];


// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledColors = shuffle(COLORS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray, num) {
  if (num === 1) {
    let number = 1;

    document.getElementById("start").addEventListener("click", startFunction);
    document.getElementById("restart").addEventListener("click", restartFunction);
    document.getElementById("overlay").addEventListener("click", overlook);
    for (let color of colorArray) {
      // create a new div
      const newDiv = document.createElement("div");

      // give it a class attribute for the value we are looping over
      newDiv.classList.add(color);

      // call a function handleCardClick when a div is clicked on
      newDiv.addEventListener("click", handleCardClick);
      newDiv.setAttribute("present", "0");
      newDiv.setAttribute("id", number);
      newDiv.setAttribute("title", "colors");
      number++;
      // append the div to the element with an id of game
      gameContainer.append(newDiv);
    }
    document.getElementsByTagName("body")[0].addEventListener("click", remove);
  } else {

    const divArray = document.getElementById("game").getElementsByTagName("div");
    console.log(divArray);
    for (let iterate = 0; iterate < divArray.length; iterate++) {
      divArray[iterate].setAttribute("class", colorArray[iterate]);
    }
  }
}
// TODO: Implement this function!
async function handleCardClick(event) {

  // you can use event.target to see which element was clicked
  console.log("you clicked", event.target.id);
  const color = event.target.getAttribute("class");
  if ((start === 1 || restart == 1) && successfulClick != 10 && lock != 1 && event.target.getAttribute("present") != "1") {
    if (prevColor === 'none') {

      document.getElementById("ScoreNumber").innerText = parseInt(document.getElementById("ScoreNumber").innerText) + 1;
      document.getElementById(event.target.id).style.backgroundColor = color;
      prevColor = event.target.getAttribute("class");
      prevId = event.target.getAttribute("Id");

    } else if (prevColor === color && prevId != event.target.id) {
      document.getElementById("ScoreNumber").innerText = parseInt(document.getElementById("ScoreNumber").innerText) + 1;
      document.getElementById(event.target.id).style.backgroundColor = color;
      document.getElementById(event.target.id).setAttribute("present", "1");
      document.getElementById(prevId).setAttribute("present", "1");
      prevColor = 'none';
      prevId = 'none';
      successfulClick += 2;
      if (successfulClick === 10) {
        document.getElementById("overlay").style.display = "flex";
        document.getElementById("overlay").style.justifyContent = "center";
        document.getElementById("overlay").style.alignItems = "center";
        document.getElementById("overlay").style.flexDirection = "column";
        console.log(document.getElementById("restart"));
        document.getElementById("restart").setAttribute("class", "1");
        console.log(document.getElementById("restart"));
        count = 1;
        event = 0;
      }
    } else if (prevId != event.target.id) {

      document.getElementById("ScoreNumber").innerText = parseInt(document.getElementById("ScoreNumber").innerText) + 1;
      document.getElementById(event.target.id).style.backgroundColor = color;
      lock = 1;

      setTimeout(() => {
        document.getElementById(event.target.id).style.backgroundColor = "white";
        document.getElementById(prevId).style.backgroundColor = "white";
        prevId = 'none';
        prevColor = 'none';
        lock = 0;
      }, 1000);
    }
    restart = 0;
  }
}

function startFunction() {
  start = 1;
}

function restartFunction() {

  if (document.getElementById("restart").className === "1") {
    document.getElementById("restart").className === "0";
    document.getElementById("overlay").style.display = "none";
  }
  if (start === 1 && successfulClick === 10) {
    score = parseInt(document.getElementById("ScoreNumber").innerHTML);
    if (document.getElementById("BestScoreNumber").innerHTML === "Null" || score < bestScore) {

      bestScore = document.getElementById("ScoreNumber").innerHTML;
      console.log(bestScore);
      document.getElementById("BestScoreNumber").innerHTML = bestScore;
    }
    document.getElementById("ScoreNumber").innerHTML = 0;
    restart = 1;
    successfulClick = 0;
    prevColor = 'none';
    prevId = 'none';
    const allDivs = document.getElementById("game").getElementsByTagName("div");
    for (let div of allDivs) {
      div.style.backgroundColor = "white";
    }
  } else if (start === 1) {
    document.getElementById("ScoreNumber").innerHTML = 0;
    successfulClick = 0;
    restart = 1;
    prevColor = 'none';
    prevId = 'none';
    const allDivs = document.getElementById("game").getElementsByTagName("div");
    for (let div of allDivs) {
      div.style.backgroundColor = "white";
    }
  }
  const Divs = document.getElementById("game").getElementsByTagName("div");
  for (let div of Divs) {
    div.setAttribute("present", "0");
  }
  shuffledColors = shuffle(COLORS);
  createDivsForColors(shuffledColors, 0);
}
// when the DOM loads
createDivsForColors(shuffledColors, 1);

function overlook() {
  document.getElementById("overlay").style.display = "none";
}

function remove() {
  if (count === 3) {
    console.log("yes");
    document.getElementById("overlay").style.display = "none";
    count = 0;
  } else if (count === 1) {
    count = 3;
  }
}